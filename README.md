# tree-sitter-wisp

Whitespace to Lisp ([Wisp](https://www.draketo.de/software/wisp)) grammar for [tree-sitter](https://github.com/tree-sitter/tree-sitter).
